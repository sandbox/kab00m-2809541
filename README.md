What?
=====

This is simply a module with no tests. It is a fixture for use with the
drupalci_testbot project.
